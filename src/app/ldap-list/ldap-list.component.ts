import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {UserLdap} from '../model/user-ldap';
import {MatSlideToggleChange} from '@angular/material/slide-toggle';
import {UsersService} from '../service/users.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-ldap',
  templateUrl: './ldap-list.component.html',
  styleUrls: ['./ldap-list.component.scss']
})
export class LdapListComponent implements OnInit {
  displayedColumns: string[] = ['nomComplet', 'mail', 'employeNumero'];
  dataSource = new MatTableDataSource<UserLdap>([]);
  unactiveSelected = false;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  constructor(private usersService: UsersService, private router: Router) { }
  addUser(): void{
    this.router.navigate(['/user/add']).then( (e) => {
      if (! e) {
        console.log('Navigation has failed!');
      }
    });
  }
  ngOnInit(): void {
    this.dataSource.paginator = this.paginator;
    this.getUsers();
  }
  private getUsers(): void {
    this.usersService.getUsers().subscribe(
      users => {
        if (this.unactiveSelected) {
          this.dataSource.data = users.filter( user =>
            user.active === false
          );
        } else {
          this.dataSource.data = users;
        }
    });
  }
  filterPredicate(data, filter): any {
    return !filter || data.nomComplet.toLowerCase().startsWith(filter);
  }
  applyFilter($event: KeyboardEvent): void{
    const filterValue = ($event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  unactiveChanged($event: MatSlideToggleChange): void{
    this.unactiveSelected = $event.checked;
    this.getUsers();
  }
  edit(login: string): void{
      this.router.navigate(['/user', login]).then( (e) => {
      if (! e) {
        console.log('Navigation has failed!');
      }
    });
  }
}
