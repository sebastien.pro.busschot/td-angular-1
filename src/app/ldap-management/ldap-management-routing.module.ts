import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LdapListComponent} from '../ldap-list/ldap-list.component';
import {LdapAddComponent} from '../ldap-add/ldap-add.component';
import {LdapEditComponent} from '../ldap-edit/ldap-edit.component';
import {LoginComponent} from '../security/login/login.component';
import {PageNotFoundComponent} from '../page-not-found/page-not-found.component';
import {LdapComponent} from './ldap/ldap.component';
import {AuthGuard} from '../security/auth.guard';

/*const routes: Routes = [
  { path: 'users/list', component: LdapListComponent },
  { path: 'user/add', component: LdapAddComponent},
  { path: 'user/:id', component: LdapEditComponent},
];*/

const adminRoutes: Routes = [
  {
    path: 'user',
    component: LdapComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        children: [
          { path: 'list', component: LdapListComponent },
          { path: 'dashboard', component: LdapListComponent },
          { path: 'import', component: LdapListComponent },
          { path: 'add', component: LdapAddComponent },
          { path: ':id', component: LdapEditComponent },
          { path: '', redirectTo: '/ldap/dashboard', pathMatch: 'full' },
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(adminRoutes)],
  exports: [RouterModule]
})
export class LdapManagementRoutingModule { }
