import { NgModule } from '@angular/core';

import { LdapManagementRoutingModule } from './ldap-management-routing.module';
import {NavbarComponent} from '../navbar/navbar.component';
import {LdapAddComponent} from '../ldap-add/ldap-add.component';
import {LdapListComponent} from '../ldap-list/ldap-list.component';
import {AlertComponent} from '../share/alert/alert.component';
import {LdapComponent} from './ldap/ldap.component';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AppMaterialModule} from '../app-material.module';
import {LdapEditComponent} from '../ldap-edit/ldap-edit.component';
import { ChartsModule } from 'ng2-charts';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {HttpClientInMemoryWebApiModule} from 'angular-in-memory-web-api';
import {InMemoryUsersService} from './in-memory-users.service';


@NgModule({
  declarations: [
    NavbarComponent,
    LdapComponent,
    LdapListComponent,
    LdapAddComponent,
    LdapEditComponent,
    AlertComponent,
  ],
  exports: [
    LdapComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    ChartsModule,
    AppMaterialModule,
    LdapManagementRoutingModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryUsersService, { dataEncapsulation: false }
    )
  ]
})
export class LdapManagementModule { }
